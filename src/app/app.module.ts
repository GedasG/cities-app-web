import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {RouterOutlet} from "@angular/router";
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {CityService} from './service/city.service';
import {CityListComponent} from './city-list/city-list.component';
import {CityFormComponent} from './city-form/city-form.component';

@NgModule({
  declarations: [
    AppComponent,
    CityListComponent,
    CityFormComponent
  ],
  imports: [
    BrowserModule,
    RouterOutlet,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [CityService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
