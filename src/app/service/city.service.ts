import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {City} from '../model/city';
import {Observable} from 'rxjs';

@Injectable()
export class CityService {

  private citiesUrl: string;

  constructor(private http: HttpClient) {
    this.citiesUrl = 'http://localhost:8080/cities';
  }

  public findAll(): Observable<City[]> {
    return this.http.get<City[]>(this.citiesUrl);
  }

  public get(id: Number): Observable<City> {
    return this.http.get<City>(this.citiesUrl + "/" + id);
  }

  public save(city: City) {
    return this.http.post<City>(this.citiesUrl, city);
  }
}
