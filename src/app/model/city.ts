export class City {
  id: number;
  name: string;
  photoUrl: string;

  constructor(id: number, name: string, photoUrl: string) {
    this.id = id;
    this.name = name;
    this.photoUrl = photoUrl;
  }
}
