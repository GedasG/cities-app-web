import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CityListComponent} from './city-list/city-list.component';
import {CityFormComponent} from './city-form/city-form.component';

const routes: Routes = [
  {path: 'cities', component: CityListComponent},
  {path: 'edit-city/:id', component: CityFormComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

